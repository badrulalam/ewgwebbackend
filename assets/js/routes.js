angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider, AccessLevels) {

        $stateProvider
            .state('anon', {
                abstract: true,
                template: '<ui-view/>',
                data: {
                    access: AccessLevels.anon
                }
            })

            .state('anon.login', {
                url: '/login',
                templateUrl: 'auth/login.html',
                controller: 'LoginController'
            })
            //.state('demo', {
            //  url: '/demo',
            //  templateUrl: 'user/demo.html',
            //  controller: 'DemoController'
            //})
            //.state('aboutus', {
            //  url: '/about-us',
            //  templateUrl: 'pages/about-us.html',
            //  controller: 'PagesController'
            //})
            //.state('services', {
            //  url: '/services',
            //  templateUrl: 'pages/services.html',
            //  controller: 'PagesController'
            //})
            .state('anon.register', {
                url: '/register',
                templateUrl: 'auth/register.html',
                controller: 'RegisterController'
            });

        $stateProvider
            //.state('user', {
            //  abstract: true,
            //  template: '<ui-view/>',
            //  data: {
            //    access: AccessLevels.user
            //  }
            //})
            //.state('user.messages', {
            //  url: '/messages',
            //  templateUrl: 'user/messages.html',
            //  controller: 'MessagesController'
            //})


            //----------New ROute Start---------

            //.state('admin',{
            //  url: '/admin',
            //  templateUrl: 'templates/admin/dashboard.html',
            //  controller: 'AdminDashboardController',
            //  data: {
            //    access: AccessLevels.admin
            //  }
            //})

            .state('home', {
                url: '/admin',
                templateUrl: 'templates/admin/dashboard.html',
                controller: 'AdminDashboardController',
                data: {
                    access: AccessLevels.user
                }
            })

            .state('home.new_event', {
                url: '/event/new',
                templateUrl: 'templates/admin/event/new.html',
                controller: 'AdminNewEventControlle'
            })

            .state('home.events', {
                url: '/event/index',
                templateUrl: 'templates/admin/event/index.html',
                controller: 'AdminEventsController'
            })

            .state('home.edit_event', {
                url: '/event/id/:evtid',
                templateUrl: 'templates/admin/event/edit.html',
                controller: 'AdminEditEventController'
            })


            //-----News Route Start----------------

            .state('home.new_news', {
                url: '/news/new',
                templateUrl: 'templates/admin/news/new.html',
                controller: 'AdminNewNewsController'
            })

            .state('home.news', {
                url: '/news/index',
                templateUrl: 'templates/admin/news/index.html',
                controller: 'AdminNewsIndexController'
            })

            .state('home.edit_news', {
                url: '/news/id/:id',
                templateUrl: 'templates/admin/news/edit.html',
                controller: 'AdminEditNewsController'
            })

            //------News Route End------------------


            //------Page Route Start---------------

            .state('home.new_page', {
                url: '/page/new',
                templateUrl: 'templates/admin/page/new.html',
                controller: 'AdminNewPageController'
            })

            .state('home.pages', {
                url: '/page/index',
                templateUrl: 'templates/admin/page/index.html',
                controller: 'AdminPageIndexController'
            })
            .state('home.edit_page', {
                url: '/page/id/:id',
                templateUrl: 'templates/admin/page/edit.html',
                controller: 'AdminEditPageController'
            })

            //-----Page Route End----------------


            //---------Image gallery Route start------------

            .state('home.new_imggallery', {
                url: '/imggallery/new',
                templateUrl: 'templates/admin/imggallery/new.html',
                controller: 'AdminNewImggalleryController'
            })

            .state('home.imggallery', {
                url: '/imggallery/index',
                templateUrl: 'templates/admin/imggallery/index.html',
                controller: 'AdminImggalleryIndexController'
            })

            .state('home.edit_imggallery', {
                url: '/imggallery/id/:id',
                templateUrl: 'templates/admin/imggallery/edit.html',
                controller: 'AdminEditImggalleryController'
            })

            //---------Image gallery Route end------------



        //--------Gal Image ROute Start---------------------

            .state('home.new_galimage', {
                url: '/galimage/new',
                templateUrl: 'templates/admin/galimage/new.html',
                controller: 'AdminNewGalimageController'
            })

            .state('home.galimages', {
                url: '/galimage/index',
                templateUrl: 'templates/admin/galimage/index.html',
                controller: 'AdminGalimageIndexController'
            })

            .state('home.edit_galimage', {
                url: '/galimage/id/:id',
                templateUrl: 'templates/admin/galimage/edit.html',
                controller: 'AdminEditGalimageController'
            })

        //--------Gal Image Route End-------------------------



            //--------New ROute End------------


        ;

        $urlRouterProvider.otherwise('/login');
    });