angular.module('app')
  .controller('PagesController', function($scope, Messages) {
    Messages.getAll().then(function(result) {
      $scope.messages = result.data;
    });
  });