angular.module('app')
    .controller('AdminNewPageController', function($scope,$http,$state,Options) {

        $scope.alerts =[];
        $scope.action='Add';

        $scope.title="Add New Page";


        //----Editor Start----


        //----Editor End-----




        $scope.page={}
        $scope.page.title = " ";
        $scope.page.body = " ";
        $scope.page.slug = "";



        $scope.MakeZeSlug = function(){
            $scope.page.slug = Options.convertToSlug( $scope.page.title);
        }

        $scope.CreatePage = function(){

            var queryString = {"title":$scope.page.title,"body": $scope.page.body,"slug": $scope.page.slug};

            $http.post("/page/create",queryString).

                success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log("News record created")
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a Page( '+data.title+' ).' });

                }).
                error(function(data, status, headers, config) {
                    $scope.alerts.push({ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' });
                });
        }

        $scope.addAlert = function() {
            $scope.alerts.push({msg: 'Another alert!'});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };



    });
