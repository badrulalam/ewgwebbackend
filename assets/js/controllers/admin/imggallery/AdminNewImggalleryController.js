angular.module('app')
    .controller('AdminNewImggalleryController', function($scope,$http,$state,Options) {

        $scope.alerts =[];
        $scope.action='Add';

        $scope.title="Add New Gallery";


        //----Editor Start----


        //----Editor End-----




        $scope.imggallery={}
        $scope.imggallery.name = " ";
        $scope.imggallery.sort = " ";




        //$scope.MakeZeSlug = function(){
        //    $scope.page.slug = Options.convertToSlug( $scope.page.title);
        //}

        $scope.CreateImggallery = function(){

            var queryString = {"name":$scope.imggallery.name,"sort": $scope.imggallery.sort};

            $http.post("/imggallery/create",queryString).

                success(function(data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    console.log("News record created")
                    console.log(data);
                    $scope.alerts.push({ type: 'success', msg: 'Well done! You have successfully created a Page( '+data.name+' ).' });

                }).
                error(function(data, status, headers, config) {
                    $scope.alerts.push({ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' });
                });
        }

        $scope.addAlert = function() {
            $scope.alerts.push({msg: 'Another alert!'});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };



    });
