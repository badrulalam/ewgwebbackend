//How to connect
var conn = new Strophe.Connection(
  'http://sunnahnikaah.com:5280/http-bind');
//'http://bosh.metajack.im:5280/xmpp-httpbind');

conn.connect(userJid, userPass, function (status) {
  if (status === Strophe.Status.CONNECTED) {
    $(document).trigger('connected');
  } else if (status === Strophe.Status.DISCONNECTED) {
    $(document).trigger('disconnected');
  }
  console.log('status',status);
});

//sending message
var message = $msg({to: jid,
  "type": "chat"})
  .c('body').t(body).up()
  .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});
Chat.connection.send(message);


//sending notification
var notify = $msg({to: jid, "type": "chat"})
  .c('composing', {xmlns: "http://jabber.org/protocol/chatstates"});
Chat.connection.send(notify);


//when connect then we are binding some handler
$(document).bind('connected', function () {
var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});
   Gab.connection.sendIQ(iq, Gab.on_roster);
   Gab.connection.addHandler(Gab.on_roster_changed,"jabber:iq:roster", "iq", "set");
   Gab.connection.addHandler(Gab.on_message,null, "message", "chat");
 });

 //contact add
 $(document).bind('contact_added', function (ev, data) {
       console.log("contact_added");
       var iq = $iq({type: "set"}).c("query", {xmlns: "jabber:iq:roster"})
         .c("item", data);
       Gab.connection.sendIQ(iq);

       var subscribe = $pres({to: data.jid, "type": "subscribe"});
       Gab.connection.send(subscribe);
     });


//subscript add/deny
$('#approve_dialog').dialog({
  autoOpen: false,
  draggable: false,
  modal: true,
  title: 'Subscription Request',
  buttons: {
    "Deny": function () {
      Chat.connection.send($pres({
        to: Chat.pending_subscriber,
        "type": "unsubscribed"}));
      Chat.pending_subscriber = null;

      $(this).dialog('close');
    },

    "Approve": function () {
      Chat.connection.send($pres({
        to: Chat.pending_subscriber,
        "type": "subscribed"}));

      Chat.connection.send($pres({
        to: Chat.pending_subscriber,
        "type": "subscribe"}));

      Chat.pending_subscriber = null;

      $(this).dialog('close');
    }
  }
});

//Controlling Presence
$pres().c('show').t('away').up().c('status').t('reading');


//add contact request
//"<iq type="set" xmlns="jabber:client"><query xmlns="jabber:iq:roster"><item jid="arif@sunnahnikaah.com" name="Arif"/></query></iq>"
var iq = $iq({type: "set"}).c("query", {xmlns: "jabber:iq:roster"})
  .c("item", data);
Gab.connection.sendIQ(iq);


//send subscription request
//<presence to="arif@sunnahnikaah.com" type="subscribe" xmlns="jabber:client"/>
var subscribe = $pres({to: data.jid, "type": "subscribe"});
Gab.connection.send(subscribe);


//subscription request
Deny = function () {
  Chat.connection.send($pres({to: Chat.pending_subscriber,"type": "unsubscribed"}));
  Chat.pending_subscriber = null;

  $(this).dialog('close');
}

Approve = function () {
  Chat.connection.send($pres({to: Chat.pending_subscriber,"type": "subscribed"}));

  Chat.connection.send($pres({to: Chat.pending_subscriber,"type": "subscribe"}));

  Chat.pending_subscriber = null;

  $(this).dialog('close');
}
